package agendatext1;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author RaulM
 */
public class LoginGrafic {
    //OBJECTES LOGICS
    private final Login login = new Login();
            
            
    //OBJECTES GRAFICS
    private final JFrame main = new JFrame("Login");
    private final JPanel pan = (JPanel)main.getContentPane();
    private final JTextField euser;
    private final JPasswordField epassword;
    private final JButton blogin;
    private final JButton bregistrar;
    
    public LoginGrafic(){
        main.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        main.setSize(350, 200);
        pan.setLayout(new GridBagLayout());
        
        //TITOL
        JLabel titol = new JLabel("Login");
        titol.setFont( new Font("Caladea", Font.PLAIN, 24));
        
        //Entrada usuari
        JLabel lusuari = new JLabel("Usuari:");
        lusuari.setFont( new Font("Times New Roman", Font.PLAIN, 16));
        euser = new JTextField();
        
        //ENTRADA CONTRASENYA
        JLabel lpassword = new JLabel("Contrasenya:");
        lpassword.setFont( new Font("Times New Roman", Font.PLAIN, 16));
        epassword = new JPasswordField();
        
        //Boto entrada
        blogin = new JButton("Login");
        
        //Boto registra
        bregistrar = new JButton("Registrar");

        //Panel d'entrada + boto
        JPanel panEntrada = new JPanel(new GridBagLayout());
        panEntrada.add(lusuari, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
        panEntrada.add(euser, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        panEntrada.add(lpassword, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 10), 0, 0));
        panEntrada.add(epassword, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 0), 0, 0));
        panEntrada.add(bregistrar, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
        panEntrada.add(blogin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
              
        pan.add(titol, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pan.add(panEntrada, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 20, 0, 20), 0, 0));
        
        
        //LISTENERS
        blogin.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                ferLogin();
            }
        });
        
        epassword.addKeyListener( new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent e){
                if(e.getKeyCode() == 10){
                    ferLogin();
                }
            }
        });
        
        euser.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                ferLogin();
            }
        });
        
        bregistrar.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                goRegistrar();
            }
        });
        
        this.main.setResizable(false);
        main.setLocationRelativeTo(null);
        main.setVisible(true);
    }
    private void goRegistrar(){
        //EXECUTAR FINESTRA PER REGISTRAR
        this.main.setVisible(false);
        //LI PAS ES CONTEXT PERQUE DES D'ALLA SI ME REGISTR ME SURT SURTI SA PANTALLA DE LOGIN DESPRES O SI CANCEL, PUGUI TORNAR ENRRERA
        this.main.dispose();
        new RegistrarGrafic();
    }
    private void ferLogin(){
        //JTextField lose focus
        this.main.requestFocus();
        if(!login.getControlUsuari().isLogedIn() && euser.getText().length() > 0 && epassword.getPassword().length > 0){
            //Desactivar s'entrada per si pitj intro a nes JOptionPane no surti una altre vegada es JOptionPane
            euser.setEnabled(false);
            epassword.setEnabled(false);
            
            String nom = euser.getText();
            String spass = HashGenerator.calcular(epassword.getPassword());
            epassword.setText("");
            login.login(nom, spass);
            //System.out.println(login.getUsuari());
            
            //SI S'HA LOGEAT EXECUTA SA INTERFAÇ DE S'AGENDA
            if(login.getControlUsuari().isLogedIn()){
                //Executar finestra de s'agenda pasant-li com a parametre s'usuari
                euser.setText("");
                epassword.setText("");
                this.main.dispose();
                new AgendaGrafica(login.getControlUsuari());
                //System.out.println("Login correcte");
                
            }
            else{//Si no dona error de login fallat
                JOptionPane.showMessageDialog(null, "Usuari o contrasenya no valida", "Login failed", JOptionPane.ERROR_MESSAGE);
                //Una vegada tancat es JOptionPane ja puc activar s'entrada e nou
                euser.setEnabled(true);
                epassword.setEnabled(true);
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Usuari o contrasenya buides", "Login failed" ,JOptionPane.ERROR_MESSAGE);
        }
    }
}
