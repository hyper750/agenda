/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.util.HashMap;

/**
 *
 * @author RaulM
 */
public class Fecha {
    public static int getDies(final int mes, final int any){
        int[] diames = {31, (bisiesto(any))? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        return (mes >= 1 && mes <= 12 && any >= 1000)? diames[mes-1] : -1;
    }
    
    public static String getMes(final int mes){
        String[] mesos = {"Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Septembre", "Octubre", "Nobembre", "Desembre"};
        return mesos[mes-1];
    }
    
    public static boolean bisiesto(final int any){
        if(any % 100 == 0){
            return any % 400 == 0;
        }
        return any % 4 == 0;
    }
    
    public static HashMap desmontarFecha(final String fecha){
        HashMap<String, Integer> resultat = new HashMap<String, Integer>(); 
        String actual = "";
        byte trobats = 0;
        if(fecha.contains("/")){
            for(int x = 0; x < fecha.length(); x++){
                if(fecha.charAt(x) == '/'){
                    if(trobats == 0){
                        //dies
                        resultat.put("Dia", Integer.parseInt(actual));
                        actual = "";
                    }
                    else if(trobats == 1){
                        //mesos
                        resultat.put("Mes", Integer.parseInt(actual));
                        actual = "";
                    }
                    trobats++;
                }
                else if(x == fecha.length()-1){
                    if(trobats == 2){
                        //anys
                        resultat.put("Any", Integer.parseInt(actual + fecha.charAt(x)));
                        actual = "";
                    }
                }
                else{
                    actual += fecha.charAt(x);
                }
            }
        }       
        else if(fecha.contains("-")){
            for(int x = 0; x < fecha.length(); x++){
                if(fecha.charAt(x) == '-'){
                    if(trobats == 0){
                        //dies
                        resultat.put("Any", Integer.parseInt(actual));
                        actual = "";
                    }
                    else if(trobats == 1){
                        //mesos
                        resultat.put("Mes", Integer.parseInt(actual));
                        actual = "";
                    }
                    trobats++;
                }
                else if(x == fecha.length()-1){
                    if(trobats == 2){
                        //anys
                        resultat.put("Dia", Integer.parseInt(actual + fecha.charAt(x)));
                        actual = "";
                    }
                }
                else{
                    actual += fecha.charAt(x);
                }
            }
        }
        return resultat;
    }
    
    //Desmontar fecha
    
}
