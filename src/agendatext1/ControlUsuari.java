/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.util.Objects;

/**
 *
 * @author RaulM
 */
public class ControlUsuari {
    private Usuari usuari;
    
    public ControlUsuari(){
        
    }
    
    public ControlUsuari(final Usuari arg){
        this.usuari = new Usuari(arg);
    }
    
    public ControlUsuari(final ControlUsuari cu){
        this(cu.getUsuari());
    }
    
    public void login(final Usuari arg){
        if(arg != null){
            this.usuari = new Usuari(arg);
        }
    }
    
    public void logout(){
        this.usuari = null;
    }
    
    public Usuari getUsuari(){
        return new Usuari(this.usuari);
    }
    
    public boolean isLogedIn(){
        return this.usuari != null;
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof ControlUsuari){
            return this.getUsuari().equals( ((ControlUsuari)arg).getUsuari() );
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.usuari);
        return hash;
    }
    
    @Override
    public String toString(){
        return "Control d'usuari de: " + this.getUsuari();
    }
}
