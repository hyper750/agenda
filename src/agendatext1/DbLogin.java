/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author RaulM
 */
public class DbLogin implements Logeable, DB {
    @Override
    public Usuari login(final String nick, final String contrasenya){
        Usuari usuari = null;
        Connection con = null;
        PreparedStatement st = null;
        
        ResultSet rsUsuari = null;
        ResultSet rsGrup = null;
        ResultSet rsRelacio = null;
        
        String selectUsuari = "select email from usuari where nick = ? and contrasenya = ? ";
        String selectRelacio = "select id_grup from usuari_grup where nick_usuari = ? ";
        String selectGrup = "select id, nom from grup where";
        
        String rEmail;
        ArrayList<Long> idgrups = new ArrayList<Long>();
        String rNomGrup;
        long rIdGrup;
        
        
        try{
            con = DriverManager.getConnection(HOST, USUARI, CONTRASENYA);
            
            st = con.prepareStatement(selectUsuari);
            st.setString(1, nick);
            st.setString(2, contrasenya);
            rsUsuari = st.executeQuery();
            
            if(rsUsuari.next()){
                //CREI L'USUARI
                rEmail = rsUsuari.getString("email");
                usuari = new Usuari(nick, rEmail);
                
                //AGAF TOTS ES ID'S DE GRUPS QUE FORMA PAR L'USUARI
                st = con.prepareStatement(selectRelacio);
                st.setString(1, nick);
                
                rsRelacio = st.executeQuery();
                while(rsRelacio.next()){
                    idgrups.add(rsRelacio.getLong("id_grup"));
                    selectGrup += " id = ? or";
                }
                if(idgrups.size() > 0){
                    selectGrup = selectGrup.substring(0, selectGrup.length()-3);
                    //System.out.println(selectGrup);
                    st = con.prepareStatement(selectGrup);

                    //AGAFAR TOTS ES GRUPS NOMES FENT UNA CONSULTA #EFICIENCIA xD
                    for(int x = 1; x <= idgrups.size(); x++){
                        st.setLong(x, idgrups.get(x-1));
                    }
                    //UNA VEGADA TENC TOTS ELS IDS FAIG SA CONSULTA
                    rsGrup = st.executeQuery();
                    while(rsGrup.next()){
                        rNomGrup = rsGrup.getString("nom");
                        rIdGrup = rsGrup.getLong("id");
                        usuari.addGrup( new Grup(rIdGrup, rNomGrup, true) );
                    }
                }
            }
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(st != null){
                    st.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            try{
                if(con != null){
                    con.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            try{
                if(rsUsuari != null){
                    rsUsuari.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            try{
                if(rsGrup != null){
                    rsGrup.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            try{
                if(rsRelacio != null){
                    rsRelacio.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
        }
        
        
        return usuari;
    }
}
