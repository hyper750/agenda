/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author RaulM
 */
public class HashGenerator {
    public static String calcular(final char[] entrada){
        return calcular(new String(entrada));
    }
    public static String calcular(final String entradaPass){
        String salt = "sg5uhwogin4TY$&HJy.h\"t430tipmñgR-`2´+`´+23Y";
        String algorithm = "SHA-512";
        String pass = null;
        try{
            MessageDigest md = MessageDigest.getInstance(algorithm);
            md.update(salt.getBytes("UTF-8"));
            byte[] bytes = md.digest( entradaPass.getBytes("UTF-8") );
            
            StringBuilder sb = new StringBuilder();
            for(int x = 0; x < bytes.length; x++){
                sb.append(Integer.toString( (bytes[x] & 0xff) + 0x100, 16).substring(1));
            }
            
            pass = sb.toString();
        }
        catch(NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        
        
        return pass;
    }
}
