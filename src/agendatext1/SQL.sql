/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  RaulM
 * Created: 30-oct-2017
 */

DROP DATABASE IF EXISTS agenda;
CREATE DATABASE agenda;
USE agenda;
create table usuari(
    nick varchar(50) primary key,
    email varchar(80) not null,
    contrasenya char(128) not null
);

INSERT INTO usuari values ('hyper7', 'raulmarquespalmer_150@hotmail.com', '70c213a573f8593b9746b62ba409295499b3d509395f1ab1fc14ed9a1e34cbe780aab616f17b30c86449799d8207dc023928461a42fdd11f599fee869d81ed1c');

create table grup(
    id bigint auto_increment primary key,
    nom varchar(50) not null
);

INSERT INTO grup(nom) values ('classe'), ('admin'), ('algo');

create table usuari_grup(
    nick_usuari varchar(50) not null,
    id_grup bigint not null,
    foreign key (nick_usuari) references usuari(nick) on update cascade on delete no action,
    foreign key (id_grup) references grup(id) on update cascade on delete no action,
    primary key(nick_usuari, id_grup)
);

INSERT INTO usuari_grup values('hyper7', 1);
INSERT INTO usuari_grup values('hyper7', 2);

create table event(
	id bigint auto_increment primary key,
    title varchar(60) not null,
    description varchar(256) not null,
    creador varchar(50) not null,
	fecha date not null,
    foreign key (creador) references usuari(nick) on update cascade on delete no action
);

insert into event(title, description, creador, fecha) values("Cumpleaños Raul", "Comprar-li un detallet i felicitar-lo", "hyper7", '2018-1-19');

create table event_grup(
	id_grup bigint not null,
    id_event bigint not null,
    primary key(id_grup, id_event),
    foreign key (id_grup) references grup(id) on update cascade on delete no action,
    foreign key (id_event) references event(id) on update cascade on delete no action
);

insert into event_grup values(1, 1);


select * from event;
select * from grup;
select * from event_grup;