/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.util.ArrayList;

/**
 *
 * @author RaulM
 */
public class Agenda {
    
    private final ControlUsuari cusuari;
    
    public Agenda(ControlUsuari cu){
        this.cusuari = new ControlUsuari(cu);
    }
    
    public ControlUsuari getControlUsuari(){
        return cusuari;
    }
    
    
}
