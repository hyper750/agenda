/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public class Event implements Comparable<Event>{
    private final long id;
    private String title;
    private String description;
    private String creador;
    private byte dia;
    private byte mes;
    private int any;
    
    public Event(long id, String title, String description, String creador, int dia, int mes, int any){
        this.id = id;
        this.title = title;
        this.description = description;
        this.creador = creador;
        this.dia = (byte)dia;
        this.mes = (byte)mes;
        this.any = any;
    }
    
    public Event(final Event arg){
        this(arg.getId(), arg.getTitle(), arg.getDescription(), arg.getCreador(), arg.getDia(), arg.getMes(), arg.getAny());
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCreador() {
        return creador;
    }

    public byte getDia() {
        return dia;
    }

    public byte getMes() {
        return mes;
    }

    public int getAny() {
        return any;
    }
    
    public boolean equalsFecha(int dia, int mes, int any){
        return this.getAny() == any && this.getMes() == mes && this.getDia() == dia;
    }
    
    
    
    @Override
    public int compareTo(Event arg){
        long dia = 0;
        long argDia = 0;
        //1 a 12
        for(int x = 1; x < this.getMes(); x++){
            dia += Fecha.getDies(x, this.getAny());
        }
        dia += this.getAny() * ((Fecha.bisiesto(this.getAny()))? 366 : 365);
        dia += this.getDia();
        
        //1 a 12
        for(int x = 1; x < arg.getMes(); x++){
            argDia += Fecha.getDies(x, arg.getAny());
        }
        argDia += arg.getAny() * ((Fecha.bisiesto(arg.getAny()))? 366 : 365);
        argDia += arg.getDia();
        
        return (int)(dia - argDia);
    }
    
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Event){
            return this.getId() == ((Event)arg).getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
    
    @Override
    public String toString(){
        return "ID: " + getId() + " Titol: " + getTitle() + " Description: " + getDescription() + " Fecha: " + getDia() + "/" + getMes() + "/" + getAny() + " Creador: " + getCreador();
    }
    
}
