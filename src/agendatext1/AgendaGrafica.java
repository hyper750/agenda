/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author RaulM
 */
public class AgendaGrafica {
    //Logica agenda
    private final Agenda logica;
    private final InterficieLlista interficieEvents = new InterficieLlista(this);
    
    long TEMPS_ACTUALITZACIO = 300000;
    long darrer;
    boolean seguir = true;
    
    //Grafics
    private final JFrame main = new JFrame("Agenda");
    private final JPanel pan = (JPanel)main.getContentPane();
    private final ArrayList<DiaGrafic> dies;
    private final JComboBox comboMes;
    private final JComboBox comboAny;
    private final JPanel panCal;
    
    private static final int DIES = 7;
    private static final int ANYS = 3000;
    
    public AgendaGrafica(final ControlUsuari cu){
        this.logica = new Agenda(cu);
        this.main.setSize(860, 600);
        this.pan.setLayout(new GridBagLayout());
        this.main.addWindowListener( new TancarAgenda());
        dies = new ArrayList<DiaGrafic>();
        //MENU
        MenuBar menu = new MenuBar();
        Menu file = new Menu("File");
        MenuItem logout = new MenuItem("Tancar sessió");
        file.add(logout);
        menu.add(file);
        this.main.setMenuBar(menu);
        
        //Crear control d'any i mes
        JPanel panControls = new JPanel(new GridBagLayout());
        JLabel lmes = new JLabel("Mes");
        lmes.setFont(new Font("Times New Roman", Font.PLAIN, 24));
        DefaultComboBoxModel<Integer> modelMes = new DefaultComboBoxModel<Integer>();
        comboMes = new JComboBox(modelMes);
        comboMes.setEditable(false);
        for(int x = 1; x <= 12; x++){
            modelMes.addElement(x);
        }
        JLabel lany = new JLabel("Any");
        lany.setFont(new Font("Times New Roman", Font.PLAIN, 24));
        DefaultComboBoxModel<Integer> modelAny = new DefaultComboBoxModel<Integer>();
        comboAny = new JComboBox(modelAny);
        comboAny.setEditable(false);
        for(int x = 2000; x <= ANYS; x++){
            modelAny.addElement(x);
        }
        
        panCal = new JPanel(new GridBagLayout());
        
        //CONTROLS
        panControls.add(lmes, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 20), 0, 0));
        panControls.add(comboMes, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panControls.add(lany, new GridBagConstraints(2, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 20, 0, 20), 0, 0));
        panControls.add(comboAny, new GridBagConstraints(3, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        //MAIN PANE
        pan.add(panControls, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(20, 20, 20, 20), 0, 0));
        pan.add(panCal, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 20, 20, 20), 0, 0));
        
        logout.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                AgendaGrafica.this.closeLogout();
            }
        });
        
        comboAny.addActionListener( new canviMesAny() );
        comboMes.addActionListener( new canviMesAny() );
        
        //Per defecte agafi s'any i mes actual
        int anyActual = new GregorianCalendar().get(Calendar.YEAR);
        int mesActual = (new GregorianCalendar().get(Calendar.MONTH)) + 1;
        comboAny.setSelectedItem((int)anyActual);
        comboMes.setSelectedItem((int)mesActual);
        
        //System.out.println("Usuari: " + logica.getControlUsuari().getUsuari());
        carregarGrupsEvents cge = new carregarGrupsEvents();
        cge.start();
        darrer = System.currentTimeMillis();
        
        this.main.setLocationRelativeTo(null);
        this.main.setVisible(true);
    }
    
    private class TancarAgenda extends WindowAdapter{
        @Override
        public synchronized void windowClosing(WindowEvent e){
            seguir = false;
            AgendaGrafica.this.closeLogout();
            AgendaGrafica.this.main.dispose();
        }
    }
    
    private class canviMesAny implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            int any = (int)comboAny.getSelectedItem();
            int mes = (int)comboMes.getSelectedItem();
            int y = 0;
            panCal.removeAll();
            dies.clear();
            
            for(int z = 1; z <= Fecha.getDies(mes, any); z++){
                dies.add(new DiaGrafic(z));
            }
            for(int x = 0; x < dies.size(); x++){
                if(x != 0 && x % DIES == 0){
                    y++;
                }
                panCal.add( dies.get(x), new GridBagConstraints((x % DIES), y, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
            }
            panCal.revalidate();
        }
    }
    
    private void closeLogout(){
        AgendaGrafica.this.interficieEvents.removeLlista();
        AgendaGrafica.this.logica.getControlUsuari().logout();
        AgendaGrafica.this.main.dispose();
        new LoginGrafic();
    }
    
    private final class DiaGrafic extends JComponent{
    
        private final static int RECT_WIDTH = 100;
        private final static int RECT_HEIGHT = 70;

        private final byte dia;

        public DiaGrafic(final int dia){
            super();
            this.dia = (byte)dia;

            this.addMouseListener( new MouseAdapter(){
                @Override
                public void mouseClicked(MouseEvent e){
                    //System.out.println("NUM: " + DiaGrafic.this.dia + logica.getControlUsuari().getUsuari());
                    //Obrir events des dia
                    if(!AgendaGrafica.this.interficieEvents.isCreated()){
                        AgendaGrafica.this.interficieEvents.crearLlista(dia);
                    }
                    else{
                        //Si ja esta creat vol dir que ja esta obert i no s'ha tancat
                        //I a mes s'ha clicat de nou aixi que posar es nous events d'aquest dia
                        
                    }
                }
            });
        }

        public Dimension customDimension(){
            return new Dimension(super.getParent().getBounds().width / 7, super.getParent().getBounds().height / 5);
        }

        @Override
        public void paint(Graphics g){
            super.paint(g);
            g.drawRect(0, 0, RECT_WIDTH, RECT_HEIGHT);
            g.drawString(String.valueOf(this.dia), 10, 15);
        }

        @Override
        public Dimension getPreferredSize(){
            return new Dimension(RECT_WIDTH + 2, RECT_HEIGHT + 2);
        }

        @Override
        public Dimension getMinimumSize(){
            return customDimension();
        }

        @Override
        public Dimension getMaximumSize(){
            return customDimension();
        }
    }

    private class InterficieLlista{
        private llistaEvents llista = null;

        public InterficieLlista(AgendaGrafica ag){
            
        }

        private void crearLlista(int dia){
            llista = new llistaEvents(dia);
        } 
       
        private void removeLlista(){
            if(isCreated()){
                llista.dispose();
            }
            llista = null;
        }

        private boolean isCreated(){
            return llista != null;
        }    
    }
    
    private class llistaEvents {    
        private final byte dia;

        private final JFrame main = new JFrame("Events");
        private final JPanel pan = (JPanel)main.getContentPane();
        private final JComboBox grups;
        private final JTable taulaEvents;

        public llistaEvents(int dia){
            this.dia = (byte)dia;

            pan.setLayout(new GridBagLayout());
            this.main.setSize(400, 600);
            //this.main.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);


            //Grups
            JLabel lgrups = new JLabel("Grups");
            lgrups.setFont(new Font("Times New Roman", Font.PLAIN, 24));
            DefaultComboBoxModel<String> modelGrup = new DefaultComboBoxModel<String>();
            modelGrup.addElement( "Tots" );
            for(int x = 0; x < logica.getControlUsuari().getUsuari().getGrups().size(); x++){
                modelGrup.addElement( logica.getControlUsuari().getUsuari().getGrups().get(x).getNom() );
            }
            grups = new JComboBox(modelGrup);
            grups.setEditable(false);

            //LLISTA EVENTS DES GRUP ORDENATS PER FECHA, JA ESTAN ORDENATS I QUAN AFEGEIXES UN DE NOU S'ORDENA DE NOU
            JPanel panLlista = new JPanel(new GridBagLayout());
            DefaultTableModel modelTaula = new DefaultTableModel();
            modelTaula.addColumn("Id");
            modelTaula.addColumn("Title");
            modelTaula.addColumn("Description");
            modelTaula.addColumn("Creador");

            taulaEvents = new JTable(modelTaula);
            
            //PAN LLISTA
            panLlista.add(new JScrollPane(taulaEvents), new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));


            //MAIN PAN
            pan.add(lgrups, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 20, 20, 20), 0, 0));
            pan.add(grups, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 0, 20, 20), 0, 0));
            pan.add(panLlista, new GridBagConstraints(0, 1, 2, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            
            this.main.addWindowListener( new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e){
                    main.dispose();
                    AgendaGrafica.this.interficieEvents.removeLlista();
                }
            });
            
            this.grups.addActionListener( new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    canviGrup();
                }
            });
            
            grups.setSelectedIndex(0);
            
            main.setLocationRelativeTo(null);
            main.setVisible(true);
        }
        
        private void canviGrup(){
            int seleccionat = grups.getSelectedIndex()-1;
            int mes = (Integer)comboMes.getSelectedItem();
            int any = (Integer)comboAny.getSelectedItem();
            //Posar tots es events d'aquest grup
            DefaultTableModel dtm = (DefaultTableModel)taulaEvents.getModel();
            //Llevar totes files
            for(int x = dtm.getRowCount()-1; x >= 0; x--){
                dtm.removeRow(x);
            }
            //Afegir ses noves files
            if(seleccionat > -1){
                ArrayList<Event> gevent = AgendaGrafica.this.logica.getControlUsuari().getUsuari().getGrup(seleccionat).getEvents();
                for(int x = 0; x < gevent.size(); x++){
                    Event e = gevent.get(x);
                    if(e.getDia() == this.dia && e.getMes() == mes && e.getAny() == any){
                        dtm.addRow(new Object[]{e.getId(), e.getTitle(), e.getDescription(), e.getCreador()});
                    }
                }
            }
            else{
                //ES -1 i -1 es tots
                ArrayList<Grup> gg = AgendaGrafica.this.logica.getControlUsuari().getUsuari().getGrups();
                ArrayList<Event> ee;
                //Per cada grup mostrar tots es events d'aquest dia mes i any
                for(int x = 0; x < gg.size(); x++){
                    //Agaf es events d'aquest grup
                    ee = gg.get(x).getEvents();
                    for(int y = 0; y < ee.size(); y++){
                        Event e = ee.get(y);
                        if(e.getDia() == this.dia && e.getMes() == mes && e.getAny() == any){
                            dtm.addRow(new Object[]{e.getId(), e.getTitle(), e.getDescription(), e.getCreador()});
                        }
                    }
                }
            }
        }
        
        private void dispose(){
            main.dispose();
        }
    }
    
    private class carregarGrupsEvents extends Thread{
        @Override
        public synchronized void run(){
            long ara;
            while(seguir){
                try{
                    Thread.sleep(200);
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
                ara = System.currentTimeMillis();
                if(darrer + AgendaGrafica.this.TEMPS_ACTUALITZACIO < ara){
                    darrer = ara;
                    AgendaGrafica.this.logica.getControlUsuari().getUsuari().loadGrups();
                    AgendaGrafica.this.logica.getControlUsuari().getUsuari().loadEvents();
                }
            }
        }
    }
    
}
