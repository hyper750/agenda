/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author RaulM
 */
public class DbRegistrar implements DB, Registrable{
    @Override
    public boolean registrar(final UsuariAmbPass usuari){
        boolean result = false;
        //Registrar usuari
        Connection con = null;
        PreparedStatement st = null;
        String insert = "insert into usuari values(?, ?, ?)";
        
        try{
            con = DriverManager.getConnection(HOST, USUARI, CONTRASENYA);
            st = con.prepareStatement(insert);
            st.setString(1, usuari.getNick());
            st.setString(2, usuari.getEmail());
            st.setString(3, usuari.getPass());
            
            result = st.executeUpdate() == 1;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(st != null){
                    st.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            
            try{
                if(con != null){
                    con.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
        }
        return result;
    }
    
    @Override
    public boolean nickExists(final String nick){
        boolean result = false;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String select = "select count(nick) from usuari where nick = ?";
        
        try{
            con = DriverManager.getConnection(HOST, USUARI, CONTRASENYA);
            st = con.prepareStatement(select);
            st.setString(1, nick);
            rs = st.executeQuery();
            if(rs.next()){
               result = rs.getInt(1) == 1;
            }
            else{
                result = false;
            }
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(con != null){
                    con.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            
            try{
                if(st != null){
                    st.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
            try{
                if(rs != null){
                    rs.close();
                }
            }
            catch(SQLException f){
                f.printStackTrace();
            }
        }
        return result;
    }
}
