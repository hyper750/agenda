/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author RaulM
 */
public class Usuari {
    private String nick;
    private String email;
    private ArrayList<Grup> grups;
            
    public Usuari(final String nick, final String email){
        this.nick = nick;
        this.email = email;
        this.grups = new ArrayList<Grup>();
    }
    
    public Usuari(final String nick, final String email, final ArrayList<Grup> grups){
        this.nick = nick;
        this.email = email;
        this.grups = grups;
    }
    
    public Usuari(final Usuari usuari){
        this(usuari.getNick(), usuari.getEmail(), usuari.getGrups());
    }
    public String getNick(){
        return this.nick;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public void addGrup(final Grup arg){
        grups.add(arg);
    }
    
    public void setGrup(final ArrayList<Grup> grups){
        this.grups = new ArrayList<Grup>(grups);
    }
    
    public Grup getGrup(final int arg){
        return grups.get(arg);
    }
    
    public Grup getGrup(final Grup arg){
        return getGrup(grups.indexOf(arg));
    }
    
    public ArrayList<Grup> getGrups(){
        return new ArrayList<Grup>(grups);
    }
    
    public void loadGrups(){
        grups = Singleton.getInstance().getDbGrup().carregar(this.getNick());
    }
    
    public void loadEvents(){
        for(int x = 0; x < grups.size(); x++){
            grups.get(x).carregarEvents();
        }
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Usuari){
            Usuari u = (Usuari)arg;
            return this.getNick().equals(u.getNick());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.nick);
        return hash;
    }
    
    @Override
    public String toString(){
        return "Nick: " + this.getNick() + " Email: " + this.getEmail() + " Grups: " + this.grups.toString();
    }
}
