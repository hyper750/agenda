/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author RaulM
 */
public class DbEvent implements Eventable, DB{
    public DbEvent(){
        
    }
    
    @Override
    public ArrayList<Event> carregar(final long id){
        ArrayList<Event> result = new ArrayList<Event>();
        String selectGrupEvent = "select * from event where id in (select id_event from event_grup where id_grup = ?);";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        long rid;
        String title;
        String description;
        String creador;
        String fecha;
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(HOST, USUARI, CONTRASENYA);
            st = con.prepareStatement(selectGrupEvent);
            st.setLong(1, id);
            rs = st.executeQuery();
            while(rs.next()){
                rid = rs.getLong(1);
                title = rs.getString(2);
                description = rs.getString(3);
                creador = rs.getString(4);
                HashMap<String, Integer> fechaDesmontada = Fecha.desmontarFecha(rs.getString(5));
                
                result.add(new Event(rid, title, description, creador, fechaDesmontada.get("Dia"), fechaDesmontada.get("Mes"), fechaDesmontada.get("Any")));
            }
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(con != null){
                    con.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
            try{
                if(st != null){
                    st.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
            try{
                if(rs != null){
                    rs.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        return result;
    }
    
}
