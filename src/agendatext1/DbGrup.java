/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author RaulM
 */
public class DbGrup implements Grupable, DB{
    @Override
    public ArrayList<Grup> carregar(final String nom){
        ArrayList<Grup> resultat = new ArrayList<Grup>();
        String select = "select * from grup where id in (select id_grup from usuari_grup where nick_usuari = ?);";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        long id;
        String nom1;
        //Carregar grups de l'usuari
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(HOST, USUARI, CONTRASENYA);
            st = con.prepareStatement(select);
            st.setString(1, nom);
            rs = st.executeQuery();
            while(rs.next()){
                id = rs.getLong(1);
                nom1 = rs.getString(2);
                
                resultat.add(new Grup(id, nom1, true));
            }
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(con != null){
                    con.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
            try{
                if(st != null){
                    st.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
            try{
                if(rs != null){
                    rs.close();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        
        return resultat;
    }
}
