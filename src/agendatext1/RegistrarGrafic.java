/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author RaulM
 */
public class RegistrarGrafic {
    private final Registrar obj = new Registrar();
    
    private final JFrame main = new JFrame("Registrar");
    private final JPanel pan = (JPanel)main.getContentPane();
    
    private final JTextField enick;
    private final JTextField eemail;
    private final JPasswordField epass;
    private final JPasswordField erpass;
    private final JButton bregistrar;
    
    public RegistrarGrafic(){
        this.main.setSize(350, 250);
        this.main.setLayout(new GridBagLayout());
        this.main.addWindowListener( new TancarRegistre() );

        JLabel lTitol = new JLabel("Registrar");
        lTitol.setFont(new Font("Caladea", Font.PLAIN, 24));
        
        //Pan registre amb inputs
        JPanel panRegistrar = new JPanel(new GridBagLayout());
        
        //Nick
        JLabel lnick = new JLabel("Nick:");
        lnick.setFont(new Font("Times New Roman", Font.PLAIN, 16));
        enick = new JTextField();
        
        //Email
        JLabel lemail = new JLabel("Email:");
        lemail.setFont(new Font("Times New Roman", Font.PLAIN, 16));
        eemail = new JTextField();
        
        //CONTASENYA
        JLabel lcontrasenya = new JLabel("Contasenya:");
        lcontrasenya.setFont( new Font("Times New Roman", Font.PLAIN, 16));
        epass = new JPasswordField();
        
        
        //REPEAT PASSWORD
        JLabel lrcontrasenya = new JLabel("Repetir contrasenya:");
        lrcontrasenya.setFont( new Font("Times New Roman", Font.PLAIN, 16));
        erpass = new JPasswordField();
        
        
        //Boto de registrar
        bregistrar = new JButton("Registrar");
        
        
        //PAN REGISTRE
        panRegistrar.add(lnick, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
        panRegistrar.add(enick, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        panRegistrar.add(lemail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 10), 0, 0));
        panRegistrar.add(eemail, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 0), 0, 0));
        panRegistrar.add(lcontrasenya, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 10), 0, 0));
        panRegistrar.add(epass, new GridBagConstraints(1, 2, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 0), 0, 0));
        panRegistrar.add(lrcontrasenya, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 10), 0, 0));
        panRegistrar.add(erpass, new GridBagConstraints(1, 3, 1, 1, 0.1, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 0), 0, 0));
        panRegistrar.add(bregistrar, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
        
        
        pan.add(lTitol, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 20, 0, 20), 0, 0));
        pan.add(panRegistrar, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 20, 0, 20), 0, 0));
        
        
        bregistrar.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                registrarte();
            }
        });
        
        enick.addKeyListener( new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent e){
                ifKey(e);
            }
        });
        
        eemail.addKeyListener( new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent e){
                ifKey(e);
            }
        });
        
        epass.addKeyListener( new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent e){
                ifKey(e);
            }
        });
        
        erpass.addKeyListener( new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent e){
                ifKey(e);
            }
        });
        
        this.main.setResizable(false);
        this.main.setLocationRelativeTo(null);
        this.main.setVisible(true);
    }
    
    private class TancarRegistre extends WindowAdapter{
        @Override
        public void windowClosing(WindowEvent e){
            new LoginGrafic();
            main.dispose();
        }
    }
    
    private void registrarte(){
        int result;
        this.main.requestFocus();
        //Tots es camps disabled
        enick.setEnabled(false);
        eemail.setEnabled(false);
        epass.setEnabled(false);
        erpass.setEnabled(false);
        bregistrar.setEnabled(false);
        
        UsuariAmbPass rusuari = new UsuariAmbPass(enick.getText(), eemail.getText(), HashGenerator.calcular(epass.getPassword()));
        String rpass = HashGenerator.calcular(erpass.getPassword());
        
        epass.setText("");
        erpass.setText("");
        
        if( rusuari.getPass().equals(rpass) ){
            result = obj.registrar(rusuari);
            switch(result){
                case -3:
                    crearJOptionPane("Registration failed", "S'email no es correcte", JOptionPane.ERROR_MESSAGE);
                    break;
                case -2:
                    crearJOptionPane("Registration failed", "Es nick ja existeix", JOptionPane.ERROR_MESSAGE);
                    break;
                case -1:
                    crearJOptionPane("Registration failed", "Error inesperat", JOptionPane.ERROR_MESSAGE);
                    break;
                case 0:
                    crearJOptionPane("Registred", "Usuari registrat", JOptionPane.INFORMATION_MESSAGE);
                    new LoginGrafic();
                    this.main.dispose();
                    break;
                default:
                    crearJOptionPane("Registration failed", "Error inesperat", JOptionPane.ERROR_MESSAGE);
                    break;
            }
        }
        else{
            crearJOptionPane("Registration failed", "Ses dues contrasenyes no són iguales", JOptionPane.ERROR_MESSAGE);
        }
        
        rpass = null;
        
        enick.setEnabled(true);
        eemail.setEnabled(true);
        epass.setEnabled(true);
        erpass.setEnabled(true);
        bregistrar.setEnabled(true);
    }
    
    private void ifKey(KeyEvent e){
        if(e.getKeyCode() == 10){
            registrarte();
        }
    }
    
    private void crearJOptionPane(String titol, String cos, int tipus){
        JOptionPane.showMessageDialog(null, cos, titol, tipus);
    }
}
