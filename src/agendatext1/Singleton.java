/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public class Singleton {
    private final static Singleton INSTANCE = new Singleton();  
    private Logeable login;
    private Registrable registrar;
    private Eventable dbEvent;
    private Grupable dbGrup;
    
    
    private Singleton(){
        this.login = new DbLogin();
        this.registrar = new DbRegistrar();
        this.dbEvent = new DbEvent();
        this.dbGrup = new DbGrup();
    }
    
    public static Singleton getInstance(){
        return INSTANCE;
    }
    
    public Logeable getLogin(){
        return this.login;
    }
    
    public Registrable getRegistrar(){
        return this.registrar;
    }
    
    public Eventable getDbEvent(){
        return this.dbEvent;
    }
    
    public Grupable getDbGrup(){
        return this.dbGrup;
    }
}
