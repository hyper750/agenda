/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public class Login {
    private final ControlUsuari cu;
    
    public Login(){
        cu = new ControlUsuari();
    }
    
    public ControlUsuari getControlUsuari(){
        return this.cu;
    }
    
    public void login(final String nick, final String contrasenya){
        //COMPROVA SI LES DADES SON CORRECTES
        //SI ES CORRECTE USUARI = NEW USUARI, SI NO ES NULL
        Usuari usu = Singleton.getInstance().getLogin().login(nick, contrasenya);
        this.cu.login(usu);
    }
}
