/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public class UsuariAmbPass extends Usuari{
    private final String pass;
    
    public UsuariAmbPass(final String nick, final String email, final String pass){
        super(nick, email);
        this.pass = pass;
    }
    
    public String getPass(){
        return this.pass;
    }
    
    @Override
    public String toString(){
        return super.toString() + " PASS: " + this.getPass();
    }
}
