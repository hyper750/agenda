/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author RaulM
 */
public final class Grup {
    private final long id;
    private String nom;
    private ArrayList<Event> events;
    
    public Grup(final long id, final String nom, final boolean load){
        this.id = id;
        this.nom = nom;
        if(load){
            carregarEvents();
            Collections.sort(events);
        }
        else{
            this.events = new ArrayList<Event>();
        }
    }
    
    public Grup(final long id, final String nom, final ArrayList<Event> events){
        this.id = id;
        this.nom = nom;
        this.events = new ArrayList<Event>(events);
    }
    
    public Grup(final Grup arg){
        this(arg.getId(), arg.getNom(), arg.getEvents());
    }
    
    public void carregarEvents(){
        this.events = Singleton.getInstance().getDbEvent().carregar(this.id);
    }
    
    public void addEvent(final Event arg){
        events.add(arg);
        Collections.sort(events);
    }
    
    public long getId(){
        return this.id;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public void setNom(final String arg){
        this.nom = arg;
    }
    
    public ArrayList<Event> getEvents(){
        return new ArrayList<Event>(this.events);
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Grup){
            Grup g = (Grup)arg;
            return this.getId() == g.getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
    
    @Override
    public String toString(){
        return "ID: " + this.getId() + " Nom: " + this.getNom() + " Events: " + getEvents().toString();
    }
}
