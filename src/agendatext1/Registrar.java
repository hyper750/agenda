/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public class Registrar {
    
    public Registrar(){
        
    }
    
    public int registrar(final UsuariAmbPass usuari){
        //Falta verificar sa contrasenya antes de fer es hash
        int result = -3;
        if(correctEmail(usuari.getEmail())){
            result++;
        }
        if(result == -2 && !Singleton.getInstance().getRegistrar().nickExists(usuari.getNick())){
            result++;
        }
        if(result == -1){
            Singleton.getInstance().getRegistrar().registrar(usuari);
            result++;
        }
        return result;
    }
    
    private boolean correctEmail(final String email){
        boolean haveArroba = false;
        boolean haveSufix = false;
        final String[] sufixList = {".com", ".es", ".net", ".uk", ".org", ".ca"};
        final int MIN = calcularMin(sufixList);
        final int MAX = calcularMax(sufixList);
        String sufix;
        
        if(MIN != -1 && MAX != -1){
            for(int x = 0; x < email.length(); x++){
                //IF ARROBA IS FALSE AND THE LENGTH IS BIGGER THAN 2 SEARCH FOR ARROBA
                if(!haveArroba && x >= 1 && ((int)email.charAt(x) == 64) ){
                    haveArroba = true;
                }
                //IF HAVE ARROBA IS TRUE SEARCH FOR .COM OR SOME SUFIX
                if(!haveSufix && haveArroba && ((email.length() - x) >= MIN || (email.length() - x) <= MAX)){
                    sufix = email.substring( x, email.length());
                    for (String sufix1 : sufixList) {
                        if (sufix.equals(sufix1)) {
                            haveSufix = true;
                            //System.out.println(sufix1);
                        }
                    }
                }
            }
        }
                
        return haveArroba && haveSufix;
    }
    
    private int calcularMin(final String[] sufixList){
        int min = (sufixList.length >= 1)? sufixList[0].length(): -1;
        for(int x = 1; x < sufixList.length; x++){
            if(sufixList[x].length() < min){
                min = sufixList[x].length();
            }
        }
        return min;
    }
    
    
    private int calcularMax(final String[] sufixList){
        int max = (sufixList.length >= 1)? sufixList[0].length() : -1;
        for(int x = 1; x < sufixList.length; x++){
            if(sufixList[x].length() > max){
                max = sufixList[x].length();
            }
        }
        return max;
    }
}
