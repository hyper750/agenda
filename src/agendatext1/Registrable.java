/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatext1;

/**
 *
 * @author RaulM
 */
public interface Registrable {
    public boolean registrar(final UsuariAmbPass usuari);
    public boolean nickExists(final String nick);
}
